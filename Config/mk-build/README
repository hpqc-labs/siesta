New mechanism for building.

The file 'build.mk' contains the logic for building Siesta. It
provides low-level code to act on the user-level specification
information, which is typically in the file arch.make, and structured
in four sections:

-------------------
A) Features needed.

They are selected by setting the 'WITH_XXX' variables to 1.

It is also possible to define the variable

   WITH_EXTRA_FPPFLAGS= SIESTA__FEATURE_X SIESTA__FEATURE_Y HAVE_Z

containing extra definitions for further manual control.

B) Symbols related to location of libraries.

They can refer to the location of a specific package (e.g. PSML_ROOT),
or to the actual libraries (e.g. SCALAPACK_LIBS). In some cases (such
as netCDF) several package-location symbols might be needed, and it might be necessary to
override directly the NETCDF_LIBS and NETCDF_INCFLAGS in hard cases.

C) Compiler names and options

D) Specific needs for certain files (for example, compilation without optimization)

-------------------

The contents of the build.mk file should be included after 'arch.make'
has been processed, either with a separate 'include' or by inserting
appropriate lines in arch.make itself.

Examples of arch.make files can be found in the 'sample-arch-makes' directory. 

NOTE: Makefiles using this mechanism MUST have a relevant target line BEFORE
including arch.make (and build.mk). Otherwise, optional target lines in arch.make (e.g.,
special rules for block D above, and targets in build.mk and its subordinate files, can
take precedence and confuse the building process. As an example:

    default: gen-basis ioncat
    # ...
    ARCH_MAKE=$(MAIN_OBJDIR)/arch.make
    include $(ARCH_MAKE)
    include $(MAIN_OBJDIR)/check_for_build_mk.mk
    # ...

or, if targets need to be built from information in aarch.make:

    dummy: default
    #...
    include $(MAIN_OBJDIR)/arch.make
    include $(MAIN_OBJDIR)/check_for_build_mk.mk
    PROGS:= grid2cube
    ifeq ($(WITH_NETCDF),1)
      PROGS+= grid2cdf
    endif
    default: $(PROGS)
